<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
<!--[if lt IE 7]><link rel="stylesheet" href="http://demo.spectacu.la/grassland/wp-content/themes/grassland/ie.css"    type="text/css" media="screen"/><![endif]-->
</head>
<body id="nov" class="apple firefox page">
<div class="container">
	
	
	<div id="header">
		<div class="login"></div>
		<div class="titles">
			<div class="main-page-title">
				<?php if ($site_name) { ?><h1>
		            <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a>
		          </h1><?php } ?>
			</div>

			<div class="tagline"><h2>A breath of fresh air in Blog design</h2></div>
		</div>
	</div>
	
	
	<div id="content">	<div class="post-frame">
		<span class="post-frame-top"></span>
			<div class="post-content">	<span class="header-image"></span>
				<?php print $content ?>
				<div class="clear"></div>

			</div>
			<div class="post-footer"></div>
		<span class="post-frame-bottom"></span>
	</div></div>
	
	
	
	
<div id="sidebar">
		<!-- No widgets included -->
		
		
	<div class="widget grassland-navigation-widget" id="naviagtion">
		<span class="widget-top"></span>
		<div class="widget-centre">
			<?php print $right ?>
			<?php print $left ?>
		</div>
		<span class="widget-bottom"></span>
	</div>

</div>		
	
	
	
<div id="footer">

			<span class="footer-link"></span>
						<div class="clear"><?php print $footer ?></div>
		</div>
	</div>
<?php print $closure ?>
</body>
</html>